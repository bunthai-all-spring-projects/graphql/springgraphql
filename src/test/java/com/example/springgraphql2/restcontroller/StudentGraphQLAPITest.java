package com.example.springgraphql2.restcontroller;

import com.example.springgraphql2.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.graphql.GraphQlTest;
import org.springframework.context.annotation.Import;
import org.springframework.graphql.test.tester.GraphQlTester;

@Import({StudentGraphQLAPI.class})
@GraphQlTest(StudentGraphQLAPI.class)
public class StudentGraphQLAPITest {

    @Autowired
    GraphQlTester graphQlTester;

    @Test
    void findAllStudent() {
        String query =
            """
                query {
                    students {
                        id
                        name
                        salary
                    }
                }
            """;

        graphQlTester
            .document(query)
            .execute()
            .path("students")
            .entityList(Student.class)
            .hasSizeGreaterThan(0);
    }

    @Test
    void findStudentById() {
        String query = """
                query test($id: ID) {
                    studentById(id: $id) {
                        id
                        name
                        salary                    
                    }
                }
            """;

        Student student = graphQlTester.document(query)
            .variable("id", 1)
            .execute()
            .path("studentById")
            .entity(Student.class)
            .get();

        Assertions.assertNotNull(student);
    }

}
