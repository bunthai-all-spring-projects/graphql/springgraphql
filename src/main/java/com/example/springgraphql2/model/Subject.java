package com.example.springgraphql2.model;

import org.springframework.boot.autoconfigure.kafka.KafkaProperties;

import java.util.Arrays;
import java.util.List;

public class Subject {
    private int id;
    private String field;
    private double cost;

    public Subject(int id, String field, double cost) {
        this.id = id;
        this.field = field;
        this.cost = cost;
    }

    public static final List<Subject> subject = Arrays.asList(
            new Subject(1, "Math", 200),
            new Subject(2, "Physic", 100),
            new Subject(3, "Chemistry", 50)
    );

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
