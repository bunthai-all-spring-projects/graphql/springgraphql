package com.example.springgraphql2.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Student {
    private int id;
    private String name;
    private double salary;

    private List<Subject> subjects;

    public Student(int id, String name, double salary, List<Subject> subjects) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.subjects = subjects;
    }

    public static final List<Student> students = new ArrayList<>() {{
            add(new Student(1, "John", 1200, Subject.subject));
            add(new Student(2, "Jack", 3000, Subject.subject));
            add(new Student(3, "Hawk", 2000, Subject.subject));
    }};

    public static Student getStudentById(int id) {
        return students.stream().filter(s -> s.id == id).findFirst().orElse(null);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}
