package com.example.springgraphql2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGraphql2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringGraphql2Application.class, args);
	}

}
