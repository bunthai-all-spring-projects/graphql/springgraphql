package com.example.springgraphql2;

import graphql.schema.DataFetcher;
import graphql.schema.GraphQLScalarType;
import graphql.schema.StaticDataFetcher;
import graphql.schema.idl.SchemaDirectiveWiring;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.graphql.data.query.QuerydslDataFetcher;
import org.springframework.graphql.execution.RuntimeWiringConfigurer;

import java.util.Map;

@Configuration
public class GraphQLConfiguration {

    /**
     * Build with customize type without using MutationMapping, QueryMapping or SchemaMapping
     * @return
     */
    @Bean
    public RuntimeWiringConfigurer runtimeWiringConfigurer() {

        DataFetcher dataFetcher = new StaticDataFetcher(Map.of("data", "bye")); //QuerydslDataFetcher.builder();

        return wiringBuilder -> wiringBuilder
                .type("Query", builder -> builder.dataFetcher("bye", dataFetcher));
    }


}
