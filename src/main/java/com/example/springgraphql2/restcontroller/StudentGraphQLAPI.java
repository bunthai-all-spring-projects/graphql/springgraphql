package com.example.springgraphql2.restcontroller;

import com.example.springgraphql2.model.Student;
import com.example.springgraphql2.model.Subject;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class StudentGraphQLAPI {


    @QueryMapping(name = "studentById")
    public Student studentById(@Argument int id) {
        System.out.println(Student.students);
        return Student.getStudentById(id);
    }

    @QueryMapping
    public List<Student> students() {
        return Student.students;
    }

    @MutationMapping
    public Student createStudent (
        @Argument("name") String name,
        @Argument("salary") double salary,
        @Argument("subjectIds") List<Integer> subjectIds
    ) {
        List<Subject> subjects = Subject.subject.stream().filter(s -> subjectIds.contains(s.getId())).collect(Collectors.toList());
        Student student = new Student(Student.students.size() + 1, name, salary, subjects);

        Student.students.add(student);

        return student;
    }


    @MutationMapping
    public Student createStudentWithObj(@Argument Map student) {
        return Student.students.stream().findFirst().orElse(null);
    }


    @MutationMapping
    public Student deleteStudent(@Argument int id) {
        Student student = Student.getStudentById(id);
        Student.students.removeIf(s -> s.getId() == id);
        return student;
    }

    @SchemaMapping(typeName = "Query", field = "random", value = "random")
    public Map abc() {
        return Map.of("data", "wow");
    }


}
